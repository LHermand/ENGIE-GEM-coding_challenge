# ENGIE GEM-coding-challenge


## How to launch the API

### Install the requirements

Open a terminal in the current folder (codingchallenge). Then tape and launch the following line : 
"pip install -r requirements.txt"

### Launch the API

In a terminal, tape and launch the following line :
"python3 manage.py runserver 8000"


