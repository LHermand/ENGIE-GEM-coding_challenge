from django.db import models
import pandas as pd

class Transformer(models.Model):

    @classmethod
    def create(self, json_list):
        self.input = json_list
        return self

    @classmethod      
    def transform(self):
        output = []
        for el in self.input:
            if (not el.isupper()) and el <'h' :
                output.append(10*ord(el))
            elif el.isupper() and el<'H' :
                output.append(10*ord(el))
            else:
                output.append(0)
        return output
        