import pandas as pd

def build_output(el):
    df = pd.DataFrame(el)
    return df.to_json()
