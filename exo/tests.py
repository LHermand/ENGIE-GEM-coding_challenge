from django.test import TestCase
from exo.models import Transformer

class ViewsTestCase(TestCase):
    def test_index_loads_properly(self):
        response = self.client.get('your_server_ip:8000')
        self.assertEqual(response.status_code, 404)

class ModelsTestCase(TestCase):
    def test_good_convert(self):
        A = Transformer.objects.create('["A", "h", "H", "x"]')
        B = Transformer.objects.create('["A", "h", "H", "x"]')
        self.assertEqual(A.transform(), '[650, 0, 0, 0]')
        self.assertEqual(B.transform(), "[650, 0, 0, 0]")