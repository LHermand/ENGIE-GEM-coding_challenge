import json
from exo.tools import build_output
from django.http import JsonResponse
from exo.models import Transformer
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def convert(request):
    if request.method == "POST":
        json_input_file = json.loads(request.body)
        exo = Transformer.create(json_input_file) #creation of a Transformer Model
        output = exo.transform() #transform to a list of int
        output_json = build_output(output) #convert in Json
        return JsonResponse(output, safe=False)
    else:
        return HttpResponse("Unautorised", content_type="application/json")
